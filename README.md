# Grupo 7 4k4 #

### Criterio linea base ###

Durante la semana correspondiente a cada parcial teniendo como dia limite el domingo de dicha semana


### Estrcutura de repositorio ###
ISW_ISO_4K4_G7
└──   README.md
└──	UTN_FRC_Modalidad_Ing_SW_2020.pdf
└───Practico
│   └───Trabajos Practicos evaluables
│		├──	Enunciados_Trabajos_Practicos_evaluables_2020
│   └───Trabajos Practicos no evaluables
│       ├──Ejercicios Resueltos
│ 		│ Trabajos_Practicos_Resueltos_2020.pdf
└───Teorico
│   └───Bibliografia
│   └───Clase Grabadas
│ 	└───Filminas
│ 	└───Papers
│ 	└───Trabajos Practicos
│ 		│ ISW_Lineamientos_para_trabajos_teóricos_2020


### Reglas de nombrado ###

| Item de configuracion | Regla de nombrado | Ubicacion fisica |
| --------------------- | ----------------- | ---------------- |
| Enunciados ejericios resueltos | Trabajos_Practicos_Resueltos_yyyy.pdf |	https://RodrigoFonseca@bitbucket.org/RodrigoFonseca/isw_iso_4k4_g7.git/Practico/TrabajosPracticosnoevaluables/EjerciciosResueltos |
| Enunciados trabajos practicos evaluables | Enunciados_Trabajos_Practicos_evaluable_2020.pdf | https://RodrigoFonseca@bitbucket.org/RodrigoFonseca/isw_iso_4k4_g7.git/Practico/TrabajosPracticosevaluables |
|	|	|